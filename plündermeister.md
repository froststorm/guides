# Plündermeister Guide
Herzlich willkommen zum Plündermeister Guide. Vielen Dank, dass du dich als Plündermeister einbringst.

## Allgemein
Die Rolle des Plündermeisters ist es, im Raid die Beute zu verteilen.  
Wir benutzen das Prio3 Loot System.

## Prio 3 Loot System
*Prio = Priorität*

Es gibt eine sg. Prio Liste.

Jeder Spieler trägt bis zu 3 Gegenstände auf dieser Liste ein.  
Einen Gegenstand setzt er auf Prio 1 als höchste Prio  
Einen Gegenstand setzt er auf Prio 2 als zweithöchste Prio  
Einen Gegenstand setzt er auf Prio 3 als niedrigste Prio  

Diese Gegenstände sind bis zum Ende des Raids die Prio 3 des Spielers.

Folgende Fälle können eintreten, wen ein Gegenstand dropt:
* Der Gegenstand ist bei niemanden auf Prio (kein Spieler hat es auf die Prio Liste gesetzt) - Alle Spieler die den Gegenstand benötigen, können darum Würfeln - der höchste Wurf gewinnt.  
* Der Gegensatnd ist bei nur einem Spieler auf irgendeine Prio gesetzt - Dieser Spieler bekommt den Gegenstand  
* Der Gegenstand ist bei mehreren Spielern auf unterschiedlichen Prios - Der Spieler, der den Gegenstand auf der höheren Prio hat, bekommt den Gegenstand
* Der Gegenstand ist bei mehreren Spielern auf der gleichen Prio - Alle Spieler mit der gleichen Prio würfeln um den Gegenstand


### Würfeln
* Eine Würfelrunde wird vom Plündermeister eröffnet.
* Jeder Spieler darf pro Würfelrunde einmal Würfeln.
* Der Spieler mit dem höchsten Wurf gewinnt.  
* Haben mehrere Spieler den gleichen höchsten Wurf, würfeln nur diese Spieler erneut.
* Es wird solange gewürfelt, bis ein Gewinner feststeht.

### Frei für alle (FFA)
Ist ein Gegenstand FFA, können alle Spieler darauf würfeln.  
Ein Gegensatnd ist FFA, wenn ihn kein Spieler auf die Prio Liste gesetzt hat und der Plündermeister ihn als FFA frei gibt (siehe dazu Besonderheiten).   
 

### Lootverteilung
Diese Lootverteilung gilt für alle FFA Gegenstände
* Jeder Spieler startet den Raid mit einer Anzahl von 0 (FFA Gegenstände) 
* Bekommt ein Spieler einen FFA Gegenstand, wird die Anzahl um 1 erhöht
* Wird um einen FFA Gegenstand gewürfelt, wird der Spieler, mit der niedrigsten Anzahl, zum Gewinner ernannt
* Haben alle Spieler die gleiche Anzahl, wird der Gewinner des Würfelns zum Gewinner ernannt
* Der Gewinner bekommt den Gegenstand

##### Beispiele:

###### Szenario 1:
*FFA Gegenstände:*  
Spieler A Anzahl 0  
Spieler B Anzahl 0  
Spieler C Anzahl 0  

Spieler A, B und C würfeln um einen FFA Gegenstand.  

Spieler A würfelt eine 45  
Spieler B würfelt eine 10  
Spieler C würfelt eine 5  

In diesem Fall bekommt Spieler A den Gegenstand, weil alle Spieler die gleiche Anzahl haben und sein Wurf höher ist als der von Spieler B und Spieler C.

###### Szenario 2:
*FFA Gegenstände:*  
Spieler A Anzahl 1  
Spieler B Anzahl 0  
Spieler C Anzahl 0  

Spieler A, B und C würfeln um einen FFA Gegenstand.  

Spieler A würfelt eine 45  
Spieler B würfelt eine 10  
Spieler C würfelt eine 5  

In diesem Fall bekommt Spieler B den Gegenstand, weil seine Anzahl niedriger ist als die von Spieler A und sein Wurf höher ist als der von Spieler C.

###### Szenario 3:
*FFA Gegenstände:*  
Spieler A Anzahl 1  
Spieler B Anzahl 1  
Spieler C Anzahl 0  

Spieler A, B und C würfeln um einen FFA Gegenstand.  

Spieler A würfelt eine 45  
Spieler B würfelt eine 10  
Spieler C würfelt eine 5  

In diesem Fall bekommt Spieler C den Gegenstand, weil seine Anzahl niedriger ist als die von Spieler A und Spieler B.

## Addons
### Prio3

https://www.curseforge.com/wow/addons/prio3

#### Prio3 - Config - Abfragen
![Abfragen](media/prio3-settings/Prio3-Settings-Abfragen.jpg)
#### Prio3 - Config - Ausgaben
![Ausgaben](media/prio3-settings/Prio3-Settings-Ausgaben.jpg)
#### Prio3 - Config - Import
![Import](media/prio3-settings/Prio3-Settings-Import.jpg)
#### Prio3 - Config - Sync & Handler
![Sync-Handler](media/prio3-settings/Prio3-Settings-Sync-Handler.jpg)

## Besonderheiten
Klassenbücher kommen auf die Gildenbank.
Raidteilnehmer können gedroppte Klassenbücher anfragen.